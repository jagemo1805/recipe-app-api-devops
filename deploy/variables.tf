variable "prefix" {
  type        = string
  default     = "raad"
  description = "description"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "mail@mail.com"
}
